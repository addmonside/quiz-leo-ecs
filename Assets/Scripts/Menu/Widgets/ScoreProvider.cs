using Common.Services.Score;
using Gameplay.Score.Widgets;
using UnityEngine;
using Zenject;

namespace Menu.Widgets
{
    [RequireComponent(typeof(IScoreWidget))]
    public class ScoreProvider : MonoBehaviour
    {
        private IScoreService _scoreService;

        [Inject]
        private void Construct(IScoreService scoreService)
        {
            _scoreService = scoreService;
        }

        private void Start()
        {
            var widget = GetComponent<IScoreWidget>();
            widget.SetScore(_scoreService.Score);
        }
    }
}

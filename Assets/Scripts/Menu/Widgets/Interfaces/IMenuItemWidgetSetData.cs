using Menu.Data;

namespace Menu.Widgets.Interfaces
{
    public interface IMenuItemWidgetSetData
    {
        void SetData(MenuItemData data);
    }
}
using System;
using UnityEngine.AddressableAssets;

namespace Menu.Widgets.Interfaces
{
    public interface IMenuItemWidgetOnClick
    {
        Action<AssetReference> OnClick { get; set; }
    }
}
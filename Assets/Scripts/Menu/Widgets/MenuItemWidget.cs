using System;
using Common.Services.GameBehaviour;
using Common.Services.Questions;
using Common.Widgets;
using Menu.Data;
using Menu.Widgets.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Localization;
using Zenject;

namespace Menu.Widgets
{
    public class MenuItemWidget : ButtonWidget, IMenuItemWidgetSetData, IMenuItemWidgetOnClick
    {
        public Action<AssetReference> OnClick { get; set; }
        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _description;
        private LocalizedString _localizedTitle;
        private LocalizedString _localizedDescription;
        private AssetReference _questionData;
        
        private QuestionsService _questionsService;
        private GameBehaviourService _gameBehaviourService;

        [Inject]
        public void Construct(QuestionsService questionsService, GameBehaviourService gameBehaviourService)
        {
            _questionsService = questionsService;
            _gameBehaviourService = gameBehaviourService;
        }
        
        void IMenuItemWidgetSetData.SetData(MenuItemData data)
        {
            _localizedTitle = data.Title;
            _localizedDescription = data.Description;
            _questionData = data.QuestionData;
            SubscribeOnChangeLanguage(_localizedTitle, UpdateTitle);
            SubscribeOnChangeLanguage(_localizedDescription, UpdateDescription);
        }

        private static void SubscribeOnChangeLanguage(in LocalizedString str, LocalizedString.ChangeHandler action)
        {
            if (str != null)
            {
                str.StringChanged += action;
            }
        }

        private void UpdateTitle(string value)
        {
            _title.text = value;
        }

        private void UpdateDescription(string value)
        {
            _description.text = value;
        }

        private void OnDisable()
        {
            Unsubscribe(_localizedTitle, UpdateTitle);
            Unsubscribe(_localizedDescription, UpdateDescription);
        }

        private static void Unsubscribe(in LocalizedString str, LocalizedString.ChangeHandler action)
        {
            if (str != null)
            {
                str.StringChanged -= action;
            }
        }

        protected override async void Click()
        {
            await _questionsService.SetAsset(_questionData);
            _gameBehaviourService.GoToGameplay();
        }
    }
}
using Menu.Data;
using Menu.Startup;
using UnityEngine;
using Zenject;

namespace Menu.Widgets
{
    public class MenuWidget : MonoBehaviour
    {
        [SerializeField] private MenuData _data;
        [SerializeField] private Transform _itemContainer;
        [SerializeField] private GameObject _itemWidget;
        private ItemFactory _itemFactory;

        [Inject]
        private void Construct(ItemFactory itemFactory)
        {
            _itemFactory = itemFactory;
        }
        private void Start()
        {
            foreach (var data in _data.Items)
            {
                var obj = _itemFactory.Create(_itemWidget, _itemContainer);
                obj.SetData(data);
            }
        }
    }
}
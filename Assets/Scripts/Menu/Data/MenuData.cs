using UnityEngine;

namespace Menu.Data
{
    [CreateAssetMenu(fileName = "QuestionsData", menuName = "Quiz/MenuData", order = 3)]
    public class MenuData : ScriptableObject
    {
        public MenuItemData[] Items;
    }
}

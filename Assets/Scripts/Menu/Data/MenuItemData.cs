using System;
using UnityEngine.AddressableAssets;
using UnityEngine.Localization;

namespace Menu.Data
{
    [Serializable]
    public class MenuItemData
    {
        public LocalizedString Title;
        public LocalizedString Description;
        public AssetReference QuestionData;
    }
}
using Common.Services.Questions;
using Menu.Widgets.Interfaces;
using UnityEngine;
using Zenject;

namespace Menu.Startup
{
    public class ItemFactory : PlaceholderFactory<QuestionsService, Transform, IMenuItemWidgetSetData>
    {
        private readonly DiContainer _container;

        public ItemFactory(DiContainer container)
        {
            _container = container;
        }


        public IMenuItemWidgetSetData Create(Object prefab, Transform parent)
        {
            var obj = _container.InstantiatePrefab(prefab, parent);
            return obj.GetComponent<IMenuItemWidgetSetData>();
            
        }
    }
}
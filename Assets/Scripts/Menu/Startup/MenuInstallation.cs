using Common.Services.Questions;
using Menu.Widgets.Interfaces;
using UnityEngine;
using Zenject;

namespace Menu.Startup
{
    public class MenuInstallation : MonoInstaller
    {
        public override void InstallBindings()
        {
            BindMenuItemFactory();
        }

        private void BindMenuItemFactory()
        {
            Container
                .BindFactory<QuestionsService, Transform, IMenuItemWidgetSetData, ItemFactory>()
                .FromFactory<ItemFactory>()
                ;
        }
    }
}
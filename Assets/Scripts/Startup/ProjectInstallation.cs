using Asp.Runtime;
using Common.Services.AssetLoading.AssetLoaders;
using Common.Services.Audio;
using Common.Services.GameBehaviour;
using Common.Services.Localization;
using Common.Services.Questions;
using Common.Services.Repository;
using Common.Services.Repository.Repository;
using Common.Services.SceneLoading;
using Common.Services.Score;
using Settings.Startup;
using Settings.Widgets;
using UnityEngine;
using Zenject;

namespace Startup
{
    public class ProjectInstallation : MonoInstaller
    {
        public override void InstallBindings()
        {
            BindSceneLoadingService();
            BindBindQuestionsService();
            BindGameBehaviourService();
            BindScoreService();
            BindRepositoryService();
            BindRepository();
            BindLocalizationRepositoryService();
            BindAudioPlayerService();
            BindMenuSettingsWidgetFactory();
        }

        private void BindScoreService()
        {
            Container
                .BindInterfacesTo<ScoreService>()
                .AsSingle();
        }

        private void BindLocalizationRepositoryService()
        {
            Container
                .BindInterfacesTo<LocaleIndexService>()
                .AsSingle()
                .Lazy();
        }

        private void BindSceneLoadingService()
        {
            Container
                .Bind<SceneLoadingService>()
                .AsSingle()
                .NonLazy();
        }

        private void BindBindQuestionsService()
        {
            Container
                .Bind<QuestionsService>()
                .AsSingle()
                .NonLazy();
        }

        private void BindGameBehaviourService()
        {
            Container
                .Bind<GameBehaviourService>()
                .AsSingle()
                .NonLazy();
        }

        private void BindRepositoryService()
        {
            Container
                .Bind<RepositoryService>()
                .AsSingle()
                .NonLazy();
        }

        private void BindRepository()
        {
            Container
                .Bind<IRepository>()
                .To<PlayerPrefsRepository>()
                .AsSingle()
                .NonLazy();
        }

        private void BindAudioPlayerService()
        {
            Container
                .BindInterfacesTo<AudioPlayerService>()
                .AsSingle()
                .NonLazy();
        }

        private void BindMenuSettingsWidgetFactory()
        {
            Container
                .BindFactory<ILocaleIndexService, IAudioParameters,  IAssetLoader<GameObject>, Transform, SettingWidget, SettingsWidgetFactory>()
                .FromFactory<SettingsWidgetFactory>();
        }
    }
}
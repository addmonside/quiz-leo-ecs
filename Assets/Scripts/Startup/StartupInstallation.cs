using Zenject;

namespace Startup
{
    public class StartupInstallation : MonoInstaller
    {
        public override void InstallBindings()
        {
            BindStartupInitialization();
        }

        private void BindStartupInitialization()
        {
            Container
                .Bind<IInitializable>()
                .To<StartupInitialization>()
                .AsSingle();
        }
    }
}

using Common.Services.GameBehaviour;
using Zenject;

namespace Startup
{
    public class StartupInitialization : IInitializable
    {
        private readonly GameBehaviourService _gameBehaviourService;
        
        public StartupInitialization(GameBehaviourService gameBehaviourService)
        {
            _gameBehaviourService = gameBehaviourService;
        }
        
        public void Initialize()
        {
            _gameBehaviourService.GoToMenu();
        }
    }
}
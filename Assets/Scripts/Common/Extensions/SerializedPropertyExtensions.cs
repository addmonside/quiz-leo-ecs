using UnityEditor;

namespace Common.Extensions
{
    public static class SerializedPropertyExtensions
    {
        public static void DrawToggle(this SerializedProperty prop)
        {
            prop.boolValue = EditorGUILayout.Toggle(prop.name, prop.boolValue);
        }
    }
}
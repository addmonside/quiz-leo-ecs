using UnityEditor;
using UnityEngine;

namespace Common.Extensions
{
    public static class RectExtensions
    {

        public static Rect GetLine(this Rect rect)
        {
            return new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight);
        }

        public static Rect GetNewLine(this Rect rect)
        {
            var y = rect.y + EditorGUIUtility.singleLineHeight + 1;
            return new Rect(rect.x, y, rect.width, EditorGUIUtility.singleLineHeight);
        }

        public static Rect GetLinePart(this Rect rect, in float width)
        {
            return new Rect(rect.x, rect.y, width, EditorGUIUtility.singleLineHeight);
        }

        public static Rect IncX(this Rect rect, float x)
        {
            return new Rect(rect.x + x, rect.y, rect.width, rect.height);
        }

        public static Rect IncY(this Rect rect, float y)
        {
            return new Rect(rect.x, rect.y + y, rect.width, rect.height);
        }

        public static Rect IncWidth(this Rect rect, float width)
        {
            return new Rect(rect.x, rect.y, rect.width + width, rect.height);
        }
    }
}
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Common.Extensions
{
    public static class ArrayExtensions
    {
        public static T[] Shuffle<T>(this IEnumerable<T> enumerable)
        {
            var list = enumerable.ToList();
            list.Sort((_, _) => Random.Range(0, 2) > 0 ? 1 : -1);
            return list.ToArray();
        } 
    }
}
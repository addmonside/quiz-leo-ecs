namespace Common.Widgets
{
    public interface IWidgetFactory<out TClass, in TEnum> where TClass: class where TEnum: struct  
    {
        public TClass Make(TEnum type);
        public void Dispose();
    }
}
using Asp.Runtime;
using UnityEngine;
using Zenject;

namespace Common.Widgets
{
    [RequireComponent(typeof(AudioSource))]
    public class SfxWidget : MonoBehaviour
    {
         private AudioSource _source;
        private ISfxPlayer _player;

        [Inject]
        private void Construct(ISfxPlayer player)
        {
            _player = player;
        }

        private void Start()
        {
            _source = GetComponent<AudioSource>();
        }

        public void Play()
        {
            _player?.Play(_source);
        }
    }
}

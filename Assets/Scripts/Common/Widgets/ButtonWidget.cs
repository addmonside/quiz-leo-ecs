using UnityEngine;
using UnityEngine.UI;

namespace Common.Widgets
{
    public abstract class ButtonWidget : MonoBehaviour
    {
        [SerializeField] private Button _button;

        private void Start()
        {
            _button.onClick.AddListener(Click);
        }

        protected abstract void Click();

        private void OnDestroy()
        {
            _button.onClick.RemoveListener(Click);
        }
        
    }
}
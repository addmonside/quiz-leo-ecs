using Asp.Runtime;
using UnityEngine;
using Zenject;

namespace Common.Widgets
{
    public class MusicWidget : MonoBehaviour
    {
        [SerializeField] private AudioSource _source;
        private IMusicPlayer _player;
        private int _id;

        [Inject]
        private void Construct(IMusicPlayer player)
        {
            _player = player;
        }

        private void OnEnable()
        {
             _id = _player.Play(_source);
        }

        private void OnDisable()
        {
            _player.Stop(_id);
        }
    }
}

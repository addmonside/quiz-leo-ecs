using System.Threading.Tasks;

namespace Common.Services.SceneLoading
{
    public class LoadingSceneLoader
    {
        private readonly string _name;
        private readonly SceneLoader _loader;
        private object _handle;

        public LoadingSceneLoader(string name, SceneLoader loader)
        {
            _name = name;
            _loader = loader;
        }

        public Task Load()
        {
            return _loader.Load(_name);
        }

        public Task Unload()
        {
            return _loader.Unload();
        }
    }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Constants;

namespace Common.Services.SceneLoading
{
    public class SceneLoadingService
    {
        private readonly LoadingSceneLoader _loadingSceneLoader;
        private readonly SceneLoader _sceneLoader;

        public SceneLoadingService()
        {
            _loadingSceneLoader = new LoadingSceneLoader(SceneNames.LoadingScene, new SceneLoader());
            _sceneLoader = new SceneLoader();
        }

        public async Task Load(string name)
        {
            await Load(name, Array.Empty<ILoadingOperation>());
        }
        public async Task Load(string name, IEnumerable<ILoadingOperation> operations)
        {
            await _loadingSceneLoader.Load();
            await _sceneLoader.Unload();
            await _sceneLoader.Load(name);
            foreach (var operation in operations)
            {
                await operation.Load();
            }
            await _loadingSceneLoader.Unload();
        }
    }
}
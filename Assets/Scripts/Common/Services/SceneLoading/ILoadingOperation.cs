using System.Threading.Tasks;

namespace Common.Services.SceneLoading
{
    public interface ILoadingOperation
    {
        Task Load();
    };
}
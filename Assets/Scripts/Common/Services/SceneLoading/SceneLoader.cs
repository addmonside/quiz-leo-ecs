using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace Common.Services.SceneLoading
{
    public class SceneLoader
    {
        private AsyncOperationHandle<SceneInstance> _handle;
        
        public Task Load(string name)
        {
            _handle = Addressables.LoadSceneAsync(name, LoadSceneMode.Additive);
            return _handle.Task;
        }

        public Task Unload()
        {
            if (_handle.IsValid())
            {
                _handle = Addressables.UnloadSceneAsync(_handle);
                return _handle.Task;
            }

            return Task.Run(()=>{});
        }
    }
}
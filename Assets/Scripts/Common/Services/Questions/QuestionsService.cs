using System.Threading.Tasks;
using Common.Extensions;
using Common.Services.AssetLoading.AssetLoaders;
using Gameplay.Question.Data;
using UnityEngine.AddressableAssets;

namespace Common.Services.Questions
{
    public class QuestionsService
    {
        public bool IsWon { get; private set; }
        private int _index;
        private IAssetLoader<QuestionsData> _assetLoader;
        private QuestionsData _questionData;

        public QuestionsService()
        {
            _index = 0;
        }

        public QuestionData GetQuestion()
        {
            return _questionData.Questions[_index];
        }
        
        public void IncrementIndex()
        {
            _index++;
            CheckForWin();
        }

        private void CheckForWin()
        {
            if (_questionData == null || _index < _questionData.Questions.Length)
            {
                return;
            }
            
            IsWon = true;
            _index--;
        }
        
        public void Dispose()
        {
            if (_assetLoader != null)
            {
                _assetLoader.Dispose();
                _assetLoader = null;
            }
            
            _index = 0;
            IsWon = false;
        }

        public async Task SetAsset(AssetReference asset)
        {
            Dispose();
            _assetLoader = new AssetLoaderByReference<QuestionsData>(asset);
            _questionData = await _assetLoader.Load();
            TryShuffleQuestions();
            TryShuffleAnswers();
        }

        private void TryShuffleQuestions()
        {
            if (_questionData.IsShuffleQuestions)
            {
                _questionData.Questions = _questionData.Questions.Shuffle();
            }
        }

        private void TryShuffleAnswers()
        {
            if (!_questionData.IsShuffleAnswers)
            {
                return;
            }
            
            foreach (var question in _questionData.Questions)
            {
                question.Answers = question.Answers.Shuffle();
            }
        }
        
    }
}
namespace Common.Services.Localization
{
    public interface ILocaleIndexService
    {
        public int LocaleIndex { get; set; }
    }
}
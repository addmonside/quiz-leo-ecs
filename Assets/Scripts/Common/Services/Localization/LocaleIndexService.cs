using System;
using Common.Services.Repository;
using UnityEngine;
using UnityEngine.Localization.Settings;
using Zenject;

namespace Common.Services.Localization
{
    [Serializable]
    public class LocaleIndexService : ILocaleIndexService, IInitializable, IDisposable
    {
        public int LocaleIndex 
        { 
            get => _localeIndex; 
            set => _localeIndex = value; 
        }
        [SerializeField]
        private int _localeIndex;
        private readonly RepositoryService _repositoryService;
        
        public LocaleIndexService(RepositoryService repositoryService)
        {
            _repositoryService = repositoryService;
        }


        public async void Initialize()
        {
            _repositoryService.Load(this);
            await LocalizationSettings.InitializationOperation.Task;
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[_localeIndex];
        }

        public void Dispose()
        {
            _repositoryService.Save(this);
        }
    }
}
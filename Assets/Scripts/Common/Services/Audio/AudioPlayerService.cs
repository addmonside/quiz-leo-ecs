using System;
using Asp.Runtime;
using Common.Services.Repository;
using Zenject;

namespace Common.Services.Audio
{
    [Serializable]
    public class AudioPlayerService :  AudioPlayer, IInitializable, IDisposable
    {
        private readonly RepositoryService _repositoryService;
        
        public AudioPlayerService(RepositoryService repositoryService)
        {
            _repositoryService = repositoryService;
            ParametersChanged += SaveParameters;
        }

        private void SaveParameters()
        {
            _repositoryService.Save(this);
        }

        public void Initialize()
        {
            _repositoryService.Load(this);
        }

        public void Dispose()
        {
            ParametersChanged -= SaveParameters;
        }
    }
}
using System.Collections.Generic;
using System.Linq;
using Common.Services.GameBehaviour.States;
using Common.Services.SceneLoading;

namespace Common.Services.GameBehaviour
{
    public class GameBehaviourService : IGameStateSwitch
    {
        private BaseGameState _prevState;
        private BaseGameState _currentState;
        private readonly List<BaseGameState> _states;

        public GameBehaviourService(SceneLoadingService sceneLoadingService)
        {
            _states = new List<BaseGameState>
            {
                new StartupState(sceneLoadingService, this),
                new MenuState(sceneLoadingService, this),
                new GameplayState(sceneLoadingService, this)
            };

            _currentState = _states[0];
        }

        public void GoToMenu()
        {
            _currentState?.GoToMenu();
        }

        public void GoToGameplay()
        {
            _currentState?.GoToGameplay();
        }

        void IGameStateSwitch.SwitchState<T>()
        {
            var state = _states.FirstOrDefault(s => s is T);
            _currentState = state;
        }
    }
}
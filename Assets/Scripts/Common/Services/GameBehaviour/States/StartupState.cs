using System.Threading.Tasks;
using Common.Constants;
using Common.Services.SceneLoading;

namespace Common.Services.GameBehaviour.States
{
    public class StartupState : BaseGameState
    {
        private readonly SceneLoadingService _sceneLoadingService;

        public StartupState(SceneLoadingService sceneLoadingService, IGameStateSwitch gameStateSwitch) : base(gameStateSwitch)
        {
            _sceneLoadingService = sceneLoadingService;
        }

        public override async Task GoToMenu()
        {
            await _sceneLoadingService.Load(SceneNames.MenuScene);
            await base.GoToMenu();
        }

        public override async Task GoToGameplay()
        {
            await _sceneLoadingService.Load(SceneNames.GameplayScene);
            await base.GoToGameplay();
        }
    }
}
using System.Threading.Tasks;
using Common.Constants;
using Common.Services.SceneLoading;

namespace Common.Services.GameBehaviour.States
{
    public class MenuState : BaseGameState
    {
        private readonly SceneLoadingService _sceneLoadingService;
        
        public MenuState(SceneLoadingService sceneLoadingService, IGameStateSwitch gameStateSwitch) : base(gameStateSwitch)
        {
            _sceneLoadingService = sceneLoadingService;
        }

        public override Task GoToMenu()
        {
            throw InvokeException();
        }

        public override async Task GoToGameplay()
        {
            await _sceneLoadingService.Load(SceneNames.GameplayScene);
            await base.GoToGameplay();
        }
    }
}
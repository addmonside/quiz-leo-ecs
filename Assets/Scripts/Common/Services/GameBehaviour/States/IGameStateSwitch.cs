namespace Common.Services.GameBehaviour.States
{
    public interface IGameStateSwitch
    {
        void SwitchState<T>() where T : BaseGameState;
    }
}
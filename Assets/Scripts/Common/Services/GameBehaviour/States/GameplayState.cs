using System.Threading.Tasks;
using Common.Constants;
using Common.Services.SceneLoading;

namespace Common.Services.GameBehaviour.States
{
    public class GameplayState : BaseGameState
    {
        private readonly SceneLoadingService _sceneLoadingService;

        public GameplayState(SceneLoadingService sceneLoadingService, IGameStateSwitch gameStateSwitch) : base(gameStateSwitch)
        {
            _sceneLoadingService = sceneLoadingService;
        }

        public override async Task GoToMenu()
        {
            await _sceneLoadingService.Load(SceneNames.MenuScene);
            GameStateSwitch.SwitchState<MenuState>();
        }

        public override Task GoToGameplay()
        {
            throw InvokeException();
        }
    }
}
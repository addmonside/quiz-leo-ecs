using System;
using System.Threading.Tasks;

namespace Common.Services.GameBehaviour.States
{
    public abstract class BaseGameState
    {
        protected readonly IGameStateSwitch GameStateSwitch;

        protected BaseGameState(IGameStateSwitch gameStateSwitch)
        {
            GameStateSwitch = gameStateSwitch;
        }

        public virtual async Task GoToMenu()
        {
            GameStateSwitch.SwitchState<MenuState>();
        }

        public virtual async Task GoToGameplay()
        {
            GameStateSwitch.SwitchState<GameplayState>();
        }

        protected static Exception InvokeException()
        {
            return new NotImplementedException();
        }
    }
}
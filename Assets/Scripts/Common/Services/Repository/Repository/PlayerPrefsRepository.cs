using UnityEngine;

namespace Common.Services.Repository.Repository
{
    public class PlayerPrefsRepository : IRepository
    {
        public bool Has(in string key)
        {
            return PlayerPrefs.HasKey(key);
        }

        public void Load<T>(in string key, in T data) where T : class
        {
            var json = PlayerPrefs.GetString(key);
            JsonUtility.FromJsonOverwrite(json, data);
        }

        public void Save<T>(in string key, in T data) where T : class
        {
            var json = JsonUtility.ToJson(data);
            PlayerPrefs.SetString(key, json);
        }
    }
}
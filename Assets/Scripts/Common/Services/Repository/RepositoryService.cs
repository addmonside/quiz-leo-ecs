using Common.Services.Repository.Repository;
using UnityEngine;

namespace Common.Services.Repository
{
    public class RepositoryService
    {
        private readonly IRepository _repository;

        public RepositoryService(IRepository repository)
        {
            _repository = repository;
        }

        public void Save<T>(T value) where T : class
        {
            var key = GetKey<T>();
            _repository.Save(key, value);
        }

        public void Load<T>(in T value) where T : class
        {
            var key = GetKey<T>();
            _repository.Load(key, value);
        }

        private static string GetKey<T>()
        {
            return $"{Application.productName}.{typeof(T)}";
        }
    }
}
namespace Common.Services.Score
{
    public interface IScoreService
    {
        int Score { get; }
        void Increment(in int score);
    }
}
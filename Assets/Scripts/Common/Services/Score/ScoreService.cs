using System;
using Common.Services.Repository;
using UnityEngine;
using Zenject;

namespace Common.Services.Score
{
    [Serializable]
    public class ScoreService : IScoreService, IInitializable, IDisposable
    {
        public int Score => _score;
        [SerializeField] private int _score;
        private readonly RepositoryService _repositoryService;
        
        public ScoreService(RepositoryService repositoryService)
        {
            _repositoryService = repositoryService;
        }

        public void Initialize()
        {
            _repositoryService.Load(this);
        }

        public void Increment(in int score = 1)
        {
            _score += score;
        }

        public void Dispose()
        {
            _repositoryService.Save(this);
        }
    }
}
using System;
using System.Threading.Tasks;

namespace Common.Services.AssetLoading.AssetLoaders
{
    public interface IAssetLoader<T> : IDisposable
    {
        Task<T> Load();
    }
}
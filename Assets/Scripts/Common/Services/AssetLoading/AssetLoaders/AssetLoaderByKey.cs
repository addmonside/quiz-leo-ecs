using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Common.Services.AssetLoading.AssetLoaders
{
    public class AssetLoaderByKey<T> : BaseAssetLoader<T>
    {
        private readonly string _key;
        
        public AssetLoaderByKey(string key)
        {
            _key = key;
        }

        protected override AsyncOperationHandle<T> GetHandle()
        {
            return Addressables.LoadAssetAsync<T>(_key);
        }
    }
}
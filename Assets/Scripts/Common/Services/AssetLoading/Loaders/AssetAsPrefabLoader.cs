using System.Threading.Tasks;
using Common.Services.AssetLoading.AssetLoaders;
using UnityEngine;

namespace Common.Services.AssetLoading.Loaders
{
    public class AssetAsPrefabLoader : IAssetLoader<GameObject>
    {
        private readonly IAssetLoader<GameObject> _loader;
        private readonly Transform _parent;

        public AssetAsPrefabLoader(IAssetLoader<GameObject> loader, Transform parent)
        {
            _loader = loader;
            _parent = parent;
        }
        
        public async Task<GameObject> Load()
        
        {
            var prefab = await _loader.Load();
            var obj = Object.Instantiate(prefab, _parent);
            return obj;
        }

        public void Dispose()
        {
            _loader.Dispose();
        }
    }
}
using System;
using System.Threading.Tasks;
using Common.Services.AssetLoading.AssetLoaders;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Common.Services.AssetLoading.Loaders
{
    public class MonoBehaviourFromAssetAsPrefabLoader<T> : IAssetLoader<T> where T : MonoBehaviour
    {
        private readonly IAssetLoader<GameObject> _loader;
        private readonly Transform _parent;

        public MonoBehaviourFromAssetAsPrefabLoader(IAssetLoader<GameObject> loader, Transform parent)
        {
            _loader = loader;
            _parent = parent;
        }

        public async Task<T> Load()
        {
            var prefab = await _loader.Load();
            var obj = Object.Instantiate(prefab, _parent);
            if (obj.TryGetComponent(out T component))
            {
                return component;
            }
            
            throw new NullReferenceException(
                $"Object of type {typeof(T)} is null o attempt to load it from addressable assets");
        }

        public void Dispose()
        {
            _loader.Dispose();
        }
    }
}
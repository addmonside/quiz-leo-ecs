using Gameplay.Win.Components;
using Gameplay.Win.Widgets;
using Voody.UniLeo;

namespace Gameplay.Win.Providers
{
    public class WinWidgetComponentProvider : MonoProvider<WinWidgetComponent>
    {
        private void Awake()
        {
            value.Value = GetComponent<IWinWidget>();
        }
    }
}
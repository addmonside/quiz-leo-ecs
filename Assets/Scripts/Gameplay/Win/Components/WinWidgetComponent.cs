using Gameplay.Win.Widgets;

namespace Gameplay.Win.Components
{
    public struct WinWidgetComponent
    {
        public IWinWidget Value;
    }
}
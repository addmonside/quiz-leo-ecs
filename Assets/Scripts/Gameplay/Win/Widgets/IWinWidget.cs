using System;

namespace Gameplay.Win.Widgets
{
    public interface IWinWidget
    {
        public event Action OnWin;
    }
}
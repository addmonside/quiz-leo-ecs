using System;
using UnityEngine;

namespace Gameplay.Win.Widgets
{
    public sealed class WinWidget : MonoBehaviour, IWinWidget
    {
        public event Action OnWin;

        public void Win()
        {
            OnWin?.Invoke();
        }

    }
}
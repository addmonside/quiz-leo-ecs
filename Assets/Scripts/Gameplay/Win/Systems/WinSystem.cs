using Common.Services.GameBehaviour;
using Gameplay.Common.Components;
using Gameplay.Win.Components;
using Leopotam.Ecs;

namespace Gameplay.Win.Systems
{
    public class WinSystem : IEcsInitSystem
    {
        private readonly EcsWorld _world;
        private readonly EcsFilter<WinWidgetComponent>.Exclude<SubscribedComponent> _filter;
        private readonly GameBehaviourService _gameBehaviourService;

        public void Init()
        {
            foreach (var index in _filter)
            {
                Subscribe(index);
            }
        }

        private void Subscribe(in int index)
        {
            ref var widget = ref _filter.Get1(index);
            widget.Value.OnWin += Win;
        }

        private void Win()
        {
            _gameBehaviourService.GoToMenu();
        }
    }
}
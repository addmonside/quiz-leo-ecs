using Leopotam.Ecs;

namespace Gameplay.Common.Components
{
    public struct SubscribedComponent : IEcsIgnoreInFilter {}
}
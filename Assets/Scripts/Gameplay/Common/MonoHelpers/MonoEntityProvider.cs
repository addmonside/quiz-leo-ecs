using Leopotam.Ecs;
using UnityEngine;
using Voody.UniLeo;

namespace Gameplay.Common.MonoHelpers
{
    [RequireComponent(typeof(ConvertToEntity))]
    public class MonoEntityProvider : MonoBehaviour
    {
        private EcsEntity _entity;
        protected EcsEntity Entity => _entity == EcsEntity.Null 
            ? _entity = GetEntity()
            : _entity ;

        private EcsEntity GetEntity()
        {
            var component = gameObject.GetComponent<ConvertToEntity>();
            return component.TryGetEntity().GetValueOrDefault();
        }

        private void OnValidate()
        {
            var component = gameObject.GetComponent<ConvertToEntity>();
            component.convertMode = ConvertMode.ConvertAndSave;
        }
    }
}
using Leopotam.Ecs;

namespace Gameplay.Common.MonoHelpers
{
    public class DestructionEntityOnDestroyGameObject : MonoEntityProvider
    {
        private void OnDestroy()
        {
            // todo: попробовать найти лучшее решение, чтобы не прилетали пустые сущности
            if (Entity == EcsEntity.Null)
                return;
            Entity.Destroy();
        }
    }
}
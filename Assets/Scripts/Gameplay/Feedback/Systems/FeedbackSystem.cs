using Gameplay.Feedback.Components;
using Leopotam.Ecs;

namespace Gameplay.Feedback.Systems
{
    public class FeedbackSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PositiveFeedbackEvent> _positiveEventFilter = null;
        private readonly EcsFilter<FeedbackComponent, PositiveFeedbackTargetComponent> _positiveFeedbackFilter = null;
        private readonly EcsFilter<NegativeFeedbackEvent> _negativeEventFilter = null;
        private readonly EcsFilter<FeedbackComponent, NegativeFeedbackTargetComponent> _negativeFeedbackFilter = null;
        private readonly EcsFilter<WinFeedbackEvent> _winEventFilter = null;
        private readonly EcsFilter<FeedbackComponent, WinFeedbackTargetComponent> _winFeedbackFilter = null;

        public void Run()
        {
            if (_winEventFilter.IsEmpty() == false)
            {
                SendFeedback(_winFeedbackFilter);
            }
            if (_positiveEventFilter.IsEmpty() == false)
            {
                SendFeedback(_positiveFeedbackFilter);
            }
            if (_negativeEventFilter.IsEmpty() == false)
            {
                SendFeedback(_negativeFeedbackFilter);
            }
        }

        private void SendFeedback(EcsFilter filter)
        {
            foreach (var index in filter)
            {
                ref var entity = ref filter.GetEntity(index);
                ref var feedback = ref entity.Get<FeedbackComponent>();
                feedback.Value.Invoke();
            }
        }
    }
}
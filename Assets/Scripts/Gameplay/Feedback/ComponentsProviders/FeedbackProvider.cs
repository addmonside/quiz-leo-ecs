using Gameplay.Feedback.Components;
using Gameplay.Feedback.Widgets;
using UnityEngine;
using Voody.UniLeo;

namespace Gameplay.Feedback.ComponentsProviders
{
    [RequireComponent(typeof(IFeedback))]
    public class FeedbackProvider : MonoProvider<FeedbackComponent>
    {
        private void Awake()
        {
            value.Value = GetComponent<IFeedback>();
        }
        
    }
}
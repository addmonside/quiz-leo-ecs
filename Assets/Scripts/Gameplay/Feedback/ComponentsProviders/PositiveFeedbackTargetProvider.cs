using Gameplay.Feedback.Components;
using Voody.UniLeo;

namespace Gameplay.Feedback.ComponentsProviders
{
    public class PositiveFeedbackTargetProvider : MonoProvider<PositiveFeedbackTargetComponent> {}
}
using UnityEngine;

namespace Gameplay.Feedback.Widgets
{
    [RequireComponent(typeof(Animator), typeof(CanvasGroup))]
    public class FeedbackWidget : MonoBehaviour, IFeedback
    {
        private Animator _animator;
        private CanvasGroup _canvasGroup;
        [SerializeField] private string _triggerShowName;
        [SerializeField] private string _triggerHideName;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _canvasGroup = GetComponent<CanvasGroup>();
            Hide();
        }

        public void Invoke()
        {
            Show();
        }

        private void Show()
        {
            _animator.SetTrigger(_triggerShowName);
            _canvasGroup.blocksRaycasts = true;
            _canvasGroup.interactable = true;
        }

        public void Hide()
        {
            _animator.SetTrigger(_triggerHideName);
            _canvasGroup.blocksRaycasts = false;
            _canvasGroup.interactable = false;
        }
    }
}
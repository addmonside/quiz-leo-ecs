namespace Gameplay.Feedback.Widgets
{
    public interface IFeedback
    {
        void Invoke();
    }
}
using UnityEngine;
using UnityEngine.Events;

namespace Gameplay.Feedback.Widgets
{
    public class Feedback : MonoBehaviour, IFeedback
    {
        [SerializeField] private UnityEvent _onFeedback;
        void IFeedback.Invoke()
        {
            _onFeedback?.Invoke();
        }
    }
}
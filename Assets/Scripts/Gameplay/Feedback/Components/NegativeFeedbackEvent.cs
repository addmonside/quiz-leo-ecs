using Leopotam.Ecs;

namespace Gameplay.Feedback.Components
{
    public struct NegativeFeedbackEvent : IEcsIgnoreInFilter {}
}
using Leopotam.Ecs;

namespace Gameplay.Feedback.Components
{
    public struct PositiveFeedbackTargetComponent : IEcsIgnoreInFilter {}
}
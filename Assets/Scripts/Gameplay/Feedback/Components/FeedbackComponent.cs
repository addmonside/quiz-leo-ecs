using Gameplay.Feedback.Widgets;

namespace Gameplay.Feedback.Components
{
    public struct FeedbackComponent
    {
        public IFeedback Value;
    }
}
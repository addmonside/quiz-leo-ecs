using System;
using UnityEngine;

namespace Gameplay.Question.Data
{
    [CreateAssetMenu(fileName = "QuestionsData", menuName = "Quiz/QuestionWidgetData", order = 1)]
    public class QuestionWidgetData : ScriptableObject
    {
        public GameObject TextWidget;
        public GameObject LocalizedTextWidget;
        public GameObject SpriteWidget;

        public GameObject Get(in QuestionType type)
        {
            return type switch
            {
                QuestionType.Text => TextWidget,
                QuestionType.LocalizedText => LocalizedTextWidget,
                QuestionType.Sprite => SpriteWidget,
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }
        
    }
}
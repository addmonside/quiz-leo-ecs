using System;
using Gameplay.Answer.Data;

namespace Gameplay.Question.Data
{
    [Serializable]
    public class QuestionData
    {
        public QuestionType QuestionType;
        public QuestionValue Value;
        public AnswerType AnswerType;
        public Answer.Data.Answer[] Answers;
    }
}

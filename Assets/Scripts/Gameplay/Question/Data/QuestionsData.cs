using UnityEngine;

namespace Gameplay.Question.Data
{
    [CreateAssetMenu(fileName = "QuestionsData", menuName = "Quiz/QuestionsData", order = 0)]
    public class QuestionsData : ScriptableObject
    {
        public QuestionData[] Questions;
        public bool IsShuffleQuestions;
        public bool IsShuffleAnswers;
    }
}
using Common.Extensions;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Gameplay.Question.Data.Editor
{
    public class QuestionsDataEditorAnswersListEditor
    {
        private readonly ReorderableList _list;
        private readonly SerializedProperty _answerType;
        private string TypeName => _answerType.enumNames[_answerType.enumValueIndex];
        
        
        public QuestionsDataEditorAnswersListEditor(SerializedObject serializedObject, SerializedProperty prop, SerializedProperty answerType)
        {
            _answerType = answerType;
            _list = new ReorderableList(serializedObject, prop, true, false, true, true)
            {
                drawElementCallback = DrawItem,
            };
        }

        private void DrawItem(Rect rect, int index, bool isActive, bool isFocused)
        {
            var prop = _list.serializedProperty.GetArrayElementAtIndex(index);
            DrawCorrectProp(rect.GetLinePart(15), prop);
            DrawPrimitiveFieldByType(rect.IncX(15).IncWidth(-15).GetLine(), prop, TypeName);
        }

        private static void DrawCorrectProp(in Rect rect, SerializedProperty prop)
        {
            var correctProp = prop.FindPropertyRelative("IsCorrect");
            correctProp.boolValue = EditorGUI.Toggle(rect, correctProp.boolValue);
        }
        
        private static void DrawPrimitiveFieldByType(in Rect rect, SerializedProperty prop, string typeName)
        {
            var valueProp = prop.FindPropertyRelative("Value").FindPropertyRelative(typeName);
            EditorGUI.PropertyField(rect, valueProp, GUIContent.none);
        }

        public void Draw(in Rect rect)
        {
            var r = rect.IncX(15).IncWidth(-15).GetLine();
            DrawAnswerType(r);
            r = r.GetNewLine();
            DrawHeader(r);
            if (_list.serializedProperty.isExpanded)
            {
                _list.DoList(r.GetNewLine());
            }
        }

        private void DrawAnswerType(in Rect rect)
        {
            var t = _answerType;
            t.enumValueIndex = EditorGUI.Popup(rect, t.name, t.enumValueIndex, t.enumNames);
        }
        private void DrawHeader(Rect rect)
        {
            var property = _list.serializedProperty;
            property.isExpanded = EditorGUI.Foldout(rect, property.isExpanded, property.name);
        }
    }
}
using Common.Extensions;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Gameplay.Question.Data.Editor
{
    public class QuestionsDataEditorQuestionListEditor
    {
        private readonly SerializedProperty _questions;
        private readonly ReorderableList _list;
        private PrimitiveType _primitiveType;
        private QuestionsDataEditorAnswersListEditor[] _answers;

        public QuestionsDataEditorQuestionListEditor(SerializedObject serializedObject, SerializedProperty questions)
        {
            _questions = questions;
            _list = new ReorderableList(serializedObject, questions, true, false, true, true)
            {
                drawElementCallback = DrawQuestion,
                elementHeightCallback = CalculateElementHeight,
                onAddCallback = AddElement
            };
            FillAnswers(_questions);
        }

        private void AddElement(ReorderableList list)
        {
            list.serializedProperty.InsertArrayElementAtIndex(_questions.arraySize);
            FillAnswers(list.serializedProperty);
        }

        private void FillAnswers(SerializedProperty questions)
        {
            _answers = new QuestionsDataEditorAnswersListEditor[questions.arraySize];
            for (var i = 0; i < _questions.arraySize; i++)
            {
                var question = questions.GetArrayElementAtIndex(i);
                var answerType = question.FindPropertyRelative("AnswerType");
                var answers = question.FindPropertyRelative("Answers");
                _answers[i] = new QuestionsDataEditorAnswersListEditor(question.serializedObject, answers, answerType);
            }
            
        }

        private void DrawQuestion(Rect rect, int index, bool isActive, bool isFocused)
        {
            if (_questions.isExpanded == false)
            {
                return;
            }
            var element = _list.serializedProperty.GetArrayElementAtIndex(index);
            var r = rect.IncX(15).IncWidth(-15).IncY(1);
            element.isExpanded = EditorGUI.Foldout(r.GetLinePart(5), element.isExpanded, GUIContent.none);
            //question type
            var questionTypeProp = element.FindPropertyRelative("QuestionType");
            questionTypeProp.enumValueIndex = EditorGUI.Popup(r.GetLinePart(95), questionTypeProp.enumValueIndex, questionTypeProp.enumNames);
            //question field
            var questionTypeName = questionTypeProp.enumNames[questionTypeProp.enumValueIndex];
            DrawPrimitiveFieldByType(r.IncX(95).IncWidth(-95).GetLine(), element, questionTypeName);
            if (element.isExpanded)
            {
                r = r.IncX(5).IncWidth(-5);
                _answers[index].Draw(r.GetNewLine());
            }
        }

        private static void DrawPrimitiveFieldByType(Rect rect, SerializedProperty prop, string typeName)
        {
            var valueProp = prop.FindPropertyRelative("Value").FindPropertyRelative(typeName);
            EditorGUI.PropertyField(rect, valueProp, GUIContent.none);
        }

        private float CalculateElementHeight(int index)
        {
            if (_questions.isExpanded == false)
            {
                return 0;
            }
            var element = _list.serializedProperty.GetArrayElementAtIndex(index);
            var answers = element.FindPropertyRelative("Answers");
            var answerCount = answers.isExpanded 
                ? (EditorGUIUtility.singleLineHeight + 6) * element.FindPropertyRelative("Answers").arraySize + 30
                : 0;
            return _questions.GetArrayElementAtIndex(index).isExpanded
                ? (EditorGUIUtility.singleLineHeight + 1) * 3 + answerCount
                : EditorGUIUtility.singleLineHeight;
        }

        public void Draw()
        {
            DrawHeader();
            if (_list.serializedProperty.isExpanded)
            {
                _list.DoLayoutList();
            }
        }
        
        private void DrawHeader()
        {
            var property = _list.serializedProperty;
            property.isExpanded = EditorGUILayout.Foldout(property.isExpanded, property.name);
        }
    }
}
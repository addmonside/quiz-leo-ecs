using Common.Extensions;
using UnityEditor;

namespace Gameplay.Question.Data.Editor
{
    [CustomEditor(typeof(QuestionsData))]
    public class QuestionsDataEditor : UnityEditor.Editor
    {
        private SerializedProperty _isShuffleQuestions;
        private SerializedProperty _isShuffleAnswers;
        private QuestionsDataEditorQuestionListEditor _list;
        
        public void OnEnable()
        {
            _isShuffleQuestions = serializedObject.FindProperty("IsShuffleQuestions");
            _isShuffleAnswers = serializedObject.FindProperty("IsShuffleAnswers");
            var questions = serializedObject.FindProperty("Questions");
            _list = new QuestionsDataEditorQuestionListEditor(serializedObject, questions);
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            _isShuffleQuestions.DrawToggle();
            _isShuffleAnswers.DrawToggle();
            _list.Draw();
            if (serializedObject.hasModifiedProperties)
            {
                serializedObject.ApplyModifiedProperties();
            }
        }
    }
}
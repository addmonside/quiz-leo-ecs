namespace Gameplay.Question.Data
{
    public enum QuestionType
    {
        Text,
        LocalizedText,
        Sprite
    }
}
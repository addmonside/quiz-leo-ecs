using System;
using UnityEngine;
using UnityEngine.Localization;

namespace Gameplay.Question.Data
{
    [Serializable]
    public class QuestionValue
    {
        public string Text;
        public LocalizedString LocalizedText;
        public Sprite Sprite;
    }
}
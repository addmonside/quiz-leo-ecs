using Gameplay.Common.Components;
using Gameplay.Question.Components;
using Leopotam.Ecs;

namespace Gameplay.Question.Systems
{
    public class QuestionActionSubscriptionSystem : IEcsRunSystem, IEcsDestroySystem
    {
        private readonly EcsWorld _world;
        private readonly EcsFilter<QuestionWidgetComponent>.Exclude<SubscribedComponent> _subscribeFilter;
        private readonly EcsFilter<QuestionWidgetComponent, SubscribedComponent> _unsubscribeFilter;
        
        public void Run()
        {
            foreach (var index in _subscribeFilter)
            {
                MarkAsSubscribed(index);
                Subscribe(index);
            }
        }

        private void MarkAsSubscribed(in int index)
        {
            ref var entity = ref _subscribeFilter.GetEntity(index);
            entity.Get<SubscribedComponent>() = new SubscribedComponent();
        }

        private void Subscribe(int index)
        {
            var widget = _subscribeFilter.Get1(index).Value;
            widget.OnNextQuestion += SendNextQuestionEvent;
        }

        private void SendNextQuestionEvent()
        {
            _world.NewEntity().Get<NextQuestionEvent>() = new NextQuestionEvent();
        }

        public void Destroy()
        {
            foreach (var index in _unsubscribeFilter)
            {
                var q = _unsubscribeFilter.Get1(index).Value;
                q.OnNextQuestion -= SendNextQuestionEvent;
            }
        }
    }
}
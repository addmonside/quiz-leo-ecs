using Common.Services.Questions;
using Gameplay.Feedback.Components;
using Gameplay.Question.Components;
using Leopotam.Ecs;

namespace Gameplay.Question.Systems
{
    public class QuestionSwitchingSystem : IEcsRunSystem, IEcsInitSystem
    {
        private readonly EcsWorld _world;
        private readonly QuestionsService _questionsService;
        private readonly EcsFilter<NextQuestionEvent> _eventFilter;
        private readonly EcsFilter<QuestionWidgetComponent> _widgetFilter;

        public void Init()
        {
            UpdateQuestion();
        }
        
        public void Run()
        {
            if (_eventFilter.IsEmpty())
            {
                return;
            }
            
            _questionsService.IncrementIndex();

            if (_questionsService.IsWon)
            {
                _questionsService.Dispose();
                SendWinFeedback();
            }
            else
            {
                UpdateQuestion();
            }
        }
       
        private void UpdateQuestion()
        {
            foreach (var index in _widgetFilter)
            {
                var widget = _widgetFilter.Get1(index).Value;
                widget.QuestionData = _questionsService.GetQuestion();
            } 
        }

        private void SendWinFeedback()
        {
            _world.NewEntity().Get<WinFeedbackEvent>() = new WinFeedbackEvent();
        }
    }
}
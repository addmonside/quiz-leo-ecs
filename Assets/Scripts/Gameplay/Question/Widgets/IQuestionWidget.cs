using System;
using Gameplay.Question.Data;

namespace Gameplay.Question.Widgets
{
    public interface IQuestionWidget
    {
        public Action OnNextQuestion { get; set; }
        public QuestionData QuestionData { set; }
    }
}
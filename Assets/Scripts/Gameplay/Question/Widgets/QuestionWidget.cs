using System;
using Gameplay.Answer.Widgets.Factory;
using Gameplay.Question.Data;
using Gameplay.Question.Widgets.Value.Factory;
using UnityEngine;

namespace Gameplay.Question.Widgets
{
    public class QuestionWidget : MonoBehaviour, IQuestionWidget
    {
        [SerializeField] private AnswerWidgetFactoryProvider _answerWidgetFactory;
        [SerializeField] private QuestionValueWidgetFactoryProvider _questionValueWidgetFactory;
        
        private QuestionData _questionData;
        public QuestionData QuestionData
        {
            set
            {
                _questionData = value;
                SetQuestion(_questionData);
            }
        }
        
        public Action OnNextQuestion { get; set; }

        private void SetQuestion(in QuestionData questionData)
        {
            var questionValueWidget = _questionValueWidgetFactory.Value.Make(questionData.QuestionType);
            questionValueWidget.Set(questionData);
            foreach (var answer in questionData.Answers)
            {
                var widget = _answerWidgetFactory.Value.Make(questionData.AnswerType);
                widget.Set(answer);
            }
        }

        public void NextQuestion()
        {
            ClearAnswersWidgets();
            OnNextQuestion?.Invoke();
        }

        private void ClearAnswersWidgets()
        {
            _questionValueWidgetFactory.Value.Dispose();
            _answerWidgetFactory.Value.Dispose();
        }

    }
}
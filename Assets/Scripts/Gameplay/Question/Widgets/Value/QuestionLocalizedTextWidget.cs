using Gameplay.Question.Data;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;

namespace Gameplay.Question.Widgets.Value
{
    public class QuestionLocalizedTextWidget : MonoBehaviour, IQuestionValueWidgetSet
    {
        [SerializeField] private TMP_Text _field;
        private LocalizedString _localizedString;
        
        public void Set(QuestionData question)
        {
            OnDisable();
            _localizedString = question.Value.LocalizedText;
            OnEnable();
        }
        
        private void OnEnable()
        {
            if (_localizedString == null)
            {
                return;
            }
            _localizedString.StringChanged += UpdateText;
        }

        private void OnDisable()
        {
            if (_localizedString == null)
            {
                return;
            }
            _localizedString.StringChanged -= UpdateText;
        }

        private void UpdateText(string value)
        {
            _field.text = value;
        }
    }
}
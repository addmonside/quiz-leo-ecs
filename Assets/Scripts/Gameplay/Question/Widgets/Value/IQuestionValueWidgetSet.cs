using Gameplay.Question.Data;

namespace Gameplay.Question.Widgets.Value
{
    public interface IQuestionValueWidgetSet
    {
        void Set(QuestionData question);
    }
}
using System.Collections.Generic;
using Common.Widgets;
using Gameplay.Question.Data;
using UnityEngine;

namespace Gameplay.Question.Widgets.Value.Factory
{
    public class QuestionValueWidgetFactory: MonoBehaviour, IWidgetFactory<IQuestionValueWidgetSet, QuestionType>
    {
        [SerializeField] private QuestionWidgetData _answerWidgetFactoryData;
        [SerializeField] private Transform _container; 
        private readonly List<GameObject> _widgets = new();
        
        public IQuestionValueWidgetSet Make(QuestionType type)
        {
            var prefab = _answerWidgetFactoryData.Get(type);
            var obj = Instantiate(prefab, _container);
            _widgets.Add(obj);
            return obj.GetComponent<IQuestionValueWidgetSet>();
        }

        void IWidgetFactory<IQuestionValueWidgetSet, QuestionType>.Dispose()
        {
            foreach (var widget in _widgets)
            {
                Destroy(widget);
            }
            _widgets.Clear();
        }
    }
}
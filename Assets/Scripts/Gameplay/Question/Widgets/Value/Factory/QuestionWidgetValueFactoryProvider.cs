using System;
using Common.Widgets;
using Gameplay.Question.Data;
using MonoInterface.Runtime;

namespace Gameplay.Question.Widgets.Value.Factory
{
    [Serializable]
    public class QuestionValueWidgetFactoryProvider : MonoInterfaceProvider<IWidgetFactory<IQuestionValueWidgetSet, QuestionType>> {}
}
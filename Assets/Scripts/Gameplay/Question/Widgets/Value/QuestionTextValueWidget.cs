using Gameplay.Question.Data;
using TMPro;
using UnityEngine;

namespace Gameplay.Question.Widgets.Value
{
    public class QuestionTextValueWidget : MonoBehaviour, IQuestionValueWidgetSet
    {
        [SerializeField] private TMP_Text _valueField;
        
        public void Set(QuestionData question)
        {
            _valueField.text = question.Value.Text;
        }
    }
}
using Gameplay.Question.Data;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.Question.Widgets.Value
{
    public class QuestionImageValueWidget : MonoBehaviour, IQuestionValueWidgetSet
    {
        [SerializeField] private Image _image;
        
        public void Set(QuestionData question)
        {
            _image.sprite = question.Value.Sprite;
        }
    }
}
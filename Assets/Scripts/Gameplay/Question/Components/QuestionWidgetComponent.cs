using Gameplay.Question.Widgets;

namespace Gameplay.Question.Components
{
    public struct QuestionWidgetComponent
    {
        public IQuestionWidget Value;
    }
}
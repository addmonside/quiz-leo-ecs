using Leopotam.Ecs;

namespace Gameplay.Question.Components
{
    public struct NextQuestionEvent : IEcsIgnoreInFilter {}
}
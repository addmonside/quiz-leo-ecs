using Gameplay.Question.Components;
using Gameplay.Question.Widgets;
using UnityEngine;
using Voody.UniLeo;

namespace Gameplay.Question.ComponentProviders
{
    [RequireComponent(typeof(IQuestionWidget))]
    public class QuestionWidgetProvider : MonoProvider<QuestionWidgetComponent>
    {
        private void Awake()
        {
            value.Value = GetComponent<IQuestionWidget>();
        }
    }
}
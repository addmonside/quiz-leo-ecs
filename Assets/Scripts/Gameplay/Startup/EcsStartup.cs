using Common.Services.GameBehaviour;
using Common.Services.Questions;
using Common.Services.Score;
using Leopotam.Ecs;
using UnityEngine;
using Zenject;

namespace Gameplay.Startup 
{
    internal sealed class EcsStartup : MonoBehaviour
    {
        private EcsWorld _world;
        private EcsSystems _systems;
        private QuestionsService _questionService;
        private GameBehaviourService _gameBehaviourService;
        private IScoreService _scoreService;

        [Inject]
        private void Construct(QuestionsService questionsService, GameBehaviourService gameBehaviourService,
            IScoreService scoreService)
        {
            _questionService = questionsService;
            _gameBehaviourService = gameBehaviourService;
            _scoreService = scoreService;
        }
        
        private void Start()
        {
            InitializeWorld();
            InitializeSystems();
        }

        private void InitializeWorld()
        {
            _world = new EcsWorld();
            #if UNITY_EDITOR
                Leopotam.Ecs.UnityIntegration.EcsWorldObserver.Create(_world);
            #endif
        }

        private void InitializeSystems()
        {
            _systems = new EcsSystems(_world);
            #if UNITY_EDITOR
                Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create(_systems);
            #endif
            _systems
                .Inject(_questionService)
                .Inject(_gameBehaviourService)
                .Inject(_scoreService)
                .InitializeStartup();
        }

        private void Update()
        {
            _systems?.Run();
        }

        private void OnDestroy() 
        {
            if (_systems != null)
            {
                _systems.Destroy();
                _systems = null;
            }

            if (_world != null)
            {
                _world.Destroy();
                _world = null;
            }
        }
    }
}
using Gameplay.Answer.Components;
using Gameplay.Answer.Systems;
using Gameplay.Feedback.Components;
using Gameplay.Feedback.Systems;
using Gameplay.Question.Components;
using Gameplay.Question.Systems;
using Gameplay.Score.Components;
using Gameplay.Score.Systems;
using Gameplay.Win.Systems;
using Leopotam.Ecs;
using Voody.UniLeo;

namespace Gameplay.Startup
{
    public static class EcsStartupInitializationSystemExtensions
    {
        private static EcsSystems InitializeSystems(this EcsSystems systems)
        {
            systems
                .Add(new WinSystem())
                .Add(new EcsSystems(systems.World, "Score")
                    .Add(new IncrementScoreSystem())
                    .OneFrame<IncrementScoreEvent>()
                )
                .Add(new EcsSystems(systems.World, "Answers")
                    .Add(new AnswerSystem())
                    .OneFrame<AnswerEventComponent>()
                    .Add(new AnswerSubscriptionSystem())
                )
                .Add(new EcsSystems(systems.World, "Questions")
                    .Add(new QuestionSwitchingSystem())
                    .OneFrame<NextQuestionEvent>()
                    .Add(new QuestionActionSubscriptionSystem())
                )
                .Add(new EcsSystems(systems.World, "Feedback")
                    .Add(new FeedbackSystem())
                    .OneFrame<PositiveFeedbackEvent>()
                    .OneFrame<NegativeFeedbackEvent>()
                    .OneFrame<WinFeedbackEvent>()
                )
                ;
            return systems;
        }

        public static EcsSystems InitializeStartup(this EcsSystems systems)
        {
            systems
                .ConvertScene()
                .InitializeSystems()
                .Init();
            return systems;
        }
    }
}
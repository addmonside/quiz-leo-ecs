using System.Collections.Generic;
using Common.Widgets;
using Gameplay.Answer.Data;
using UnityEngine;

namespace Gameplay.Answer.Widgets.Factory
{
    public class SimpleAnswerWidgetFactory: MonoBehaviour, IWidgetFactory<IAnswerWidgetSet, AnswerType>
    {
        [SerializeField] private AnswerWidgetData _answerWidgetFactoryData;
        [SerializeField] private Transform _container; 
        private readonly List<GameObject> _widgets = new();

        public IAnswerWidgetSet Make(AnswerType type)
        {
            var prefab = _answerWidgetFactoryData.Get(type);
            var obj = Instantiate(prefab, _container);
            _widgets.Add(obj);
            return obj.GetComponent<IAnswerWidgetSet>();
        }

        void IWidgetFactory<IAnswerWidgetSet, AnswerType>.Dispose()
        {
            foreach (var obj in _widgets)
            {
                Destroy(obj);
            }
            _widgets.Clear();
        }
    }
}
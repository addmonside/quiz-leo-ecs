using System;
using Common.Widgets;
using Gameplay.Answer.Data;
using MonoInterface.Runtime;

namespace Gameplay.Answer.Widgets.Factory
{
    [Serializable]
    public class AnswerWidgetFactoryProvider : MonoInterfaceProvider<IWidgetFactory<IAnswerWidgetSet, AnswerType>> {}
}
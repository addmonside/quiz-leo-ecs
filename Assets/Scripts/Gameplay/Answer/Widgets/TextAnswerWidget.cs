using System;
using Common.Widgets;
using TMPro;
using UnityEngine;

namespace Gameplay.Answer.Widgets
{
    public class TextAnswerWidget : ButtonWidget, IAnswerWidgetOnAnswer, IAnswerWidgetSet
    {
        [SerializeField] private TMP_Text _valueField;
        private bool _isCorrected;
        public Action<bool> OnAnswer { get; set; }

        public void Set(Data.Answer answer)
        {
            _valueField.text = answer.Value.Text;
            _isCorrected = answer.IsCorrect;
        }

        protected override void Click()
        {
            OnAnswer?.Invoke(_isCorrected);
        }
    }
}
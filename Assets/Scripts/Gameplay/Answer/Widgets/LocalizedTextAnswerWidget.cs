using System;
using Common.Widgets;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;

namespace Gameplay.Answer.Widgets
{
    public class LocalizedTextAnswerWidget : ButtonWidget, IAnswerWidgetOnAnswer, IAnswerWidgetSet
    {
        [SerializeField] private TMP_Text _field;
        private bool _isCorrected;
        private LocalizedString _localizedString;
        public Action<bool> OnAnswer { get; set; }
        
        public void Set(Data.Answer answer)
        {
            OnDisable();
            _localizedString = answer.Value.LocalizedText;
            OnEnable();
            _isCorrected = answer.IsCorrect;
        }
        
        private void OnEnable()
        {
            if (_localizedString == null)
            {
                return;
            }
            _localizedString.StringChanged += UpdateText;
        }

        private void OnDisable()
        {
            if (_localizedString == null)
            {
                return;
            }
            _localizedString.StringChanged -= UpdateText;
        }

        private void UpdateText(string value)
        {
            _field.text = value;
        }

        protected override void Click()
        {
            OnAnswer?.Invoke(_isCorrected);
        }
    }
}
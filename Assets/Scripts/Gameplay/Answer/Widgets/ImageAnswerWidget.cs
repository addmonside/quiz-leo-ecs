using System;
using Common.Widgets;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.Answer.Widgets
{
    public class ImageAnswerWidget : ButtonWidget, IAnswerWidgetOnAnswer, IAnswerWidgetSet
    {
        [SerializeField] private Image _image;
        private bool _isCorrected;
        
        public Action<bool> OnAnswer { get; set; }

        public void Set(Data.Answer answer)
        {
            _image.sprite = answer.Value.Sprite;
            _isCorrected = answer.IsCorrect;
        }

        protected override void Click()
        {
            OnAnswer?.Invoke(_isCorrected);
        }
    }
}
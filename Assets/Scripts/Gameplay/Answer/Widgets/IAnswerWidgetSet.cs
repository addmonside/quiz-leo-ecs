namespace Gameplay.Answer.Widgets
{
    public interface IAnswerWidgetSet
    {
        void Set(Data.Answer answer);
    }
}
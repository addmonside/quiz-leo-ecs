using System;

namespace Gameplay.Answer.Widgets
{
    public interface IAnswerWidgetOnAnswer
    {
        Action<bool> OnAnswer { get; set; }
    }
}
using Gameplay.Answer.Components;
using Gameplay.Answer.Widgets;
using UnityEngine;
using Voody.UniLeo;

namespace Gameplay.Answer.ComponentProviders
{
    [RequireComponent(typeof(IAnswerWidgetOnAnswer))]
    public class AnswerWidgetProvider : MonoProvider<AnswerWidgetComponent>
    {
        private void Awake()
        {
            value.Value = GetComponent<IAnswerWidgetOnAnswer>();
        }
    }
}
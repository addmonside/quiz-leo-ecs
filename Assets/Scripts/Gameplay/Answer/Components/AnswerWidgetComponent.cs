using Gameplay.Answer.Widgets;

namespace Gameplay.Answer.Components
{
    public struct AnswerWidgetComponent
    {
        public IAnswerWidgetOnAnswer Value;
    }
}
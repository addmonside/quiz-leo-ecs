using Gameplay.Answer.Components;
using Gameplay.Common.Components;
using Leopotam.Ecs;

namespace Gameplay.Answer.Systems
{
    public class AnswerSubscriptionSystem : IEcsRunSystem, IEcsDestroySystem
    {
        private readonly EcsWorld _world;
        private readonly EcsFilter<AnswerWidgetComponent>.Exclude<SubscribedComponent> _subscribeFilter;
        private readonly EcsFilter<AnswerWidgetComponent, SubscribedComponent> _unsubscribeFilter;
        
        public void Run()
        {
            foreach (var index in _subscribeFilter)
            {
                MarkAsSubscribed(index);
                Subscribe(index);
            }
        }

        private void MarkAsSubscribed(in int index)
        {
            ref var entity = ref _subscribeFilter.GetEntity(index);
            entity.Get<SubscribedComponent>() = new SubscribedComponent();
        }

        private void Subscribe(in int index)
        {
            var widget = _subscribeFilter.Get1(index).Value;
            widget.OnAnswer += SendAnswer;
        }
        
        private void SendAnswer(bool isCorrect)
        {
            _world.NewEntity().Get<AnswerEventComponent>() = new AnswerEventComponent
            {
                Value = isCorrect
            };
        }

        public void Destroy()
        {
            foreach (var index in _unsubscribeFilter)
            {
                var widget = _subscribeFilter.Get1(index).Value;
                widget.OnAnswer -= SendAnswer;
            }
        }
    }
}
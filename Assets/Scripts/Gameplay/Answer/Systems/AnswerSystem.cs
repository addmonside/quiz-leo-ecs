using Gameplay.Answer.Components;
using Gameplay.Feedback.Components;
using Gameplay.Score.Components;
using Leopotam.Ecs;

namespace Gameplay.Answer.Systems
{
    public class AnswerSystem : IEcsRunSystem
    {
        private readonly EcsFilter<AnswerEventComponent> _filter;
        private readonly EcsWorld _world;
        
        public void Run()
        {
            foreach (var index in _filter)
            {
                ref var answer = ref _filter.Get1(index);
                if (answer.Value)
                {
                    IncrementScore(1);
                    SendPositiveFeedBack();
                }
                else
                {
                    SendNegativeFeedback();
                }
            }
        }
        
        private void IncrementScore(int value)
        {
            _world.NewEntity().Get<IncrementScoreEvent>() = new IncrementScoreEvent
            {
                Value = value
            };
        }
        
        private void SendPositiveFeedBack()
        {
            _world.NewEntity().Get<PositiveFeedbackEvent>() = new PositiveFeedbackEvent();
        }

        private void SendNegativeFeedback()
        {
            _world.NewEntity().Get<NegativeFeedbackEvent>() = new NegativeFeedbackEvent();
        }
    }
}
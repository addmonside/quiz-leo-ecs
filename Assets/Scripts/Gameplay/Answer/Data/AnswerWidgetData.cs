using System;
using UnityEngine;

namespace Gameplay.Answer.Data
{
    [CreateAssetMenu(fileName = "QuestionsData", menuName = "Quiz/AnswerWidgetData", order = 1)]
    public class AnswerWidgetData : ScriptableObject
    {
        public GameObject TextWidget;
        public GameObject LocalizedTextWidget;
        public GameObject SpriteWidget;

        public GameObject Get(in AnswerType type)
        {
            return type switch
            {
                AnswerType.Text => TextWidget,
                AnswerType.Sprite => SpriteWidget,
                AnswerType.LocalizedText => LocalizedTextWidget,
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }
    }
}

using System;

namespace Gameplay.Answer.Data
{
    [Serializable]
    public class Answer
    {
        public bool IsCorrect;
        public AnswerValue Value;
    }
}
using System;
using UnityEngine;
using UnityEngine.Localization;

namespace Gameplay.Answer.Data
{
    [Serializable]
    public class AnswerValue
    {
        public string Text;
        public LocalizedString LocalizedText;
        public Sprite Sprite;
    }
}
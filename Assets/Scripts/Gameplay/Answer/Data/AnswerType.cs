namespace Gameplay.Answer.Data
{
    public enum AnswerType
    {
        Text,
        LocalizedText,
        Sprite
    }
}
namespace Gameplay.Score.Components
{
    internal struct IncrementScoreEvent
    {
        public int Value;
    }
}
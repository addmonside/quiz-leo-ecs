using Gameplay.Score.Widgets;

namespace Gameplay.Score.Components
{
    public struct ScoreWidgetComponent
    {
        public IScoreWidget Value;
    }
}
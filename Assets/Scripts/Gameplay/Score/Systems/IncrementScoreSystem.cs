using Common.Services.Score;
using Gameplay.Score.Components;
using Leopotam.Ecs;

namespace Gameplay.Score.Systems
{
    public class IncrementScoreSystem : IEcsRunSystem, IEcsInitSystem
    {
        private readonly IScoreService _service;
        private readonly EcsFilter<IncrementScoreEvent> _eventFilter = null;
        private readonly EcsFilter<ScoreWidgetComponent> _widgetFilter = null;
        
        public void Init()
        {
            UpdateWidgets();
        }

        private void UpdateWidgets()
        {
            foreach (var index in _widgetFilter)
            {
                var widget = _widgetFilter.Get1(index).Value;
                widget.SetScore(_service.Score);
            }
        }
        
        public void Run()
        {
            if (_eventFilter.IsEmpty())
            {
                return;
            }
            
            IncrementScore();
            UpdateWidgets();
        }

        private void IncrementScore()
        {
            foreach (var index in _eventFilter)
            {
                ref var evt = ref _eventFilter.Get1(index);
                _service.Increment(evt.Value);
            }
        }
    }
}
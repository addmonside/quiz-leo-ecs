using TMPro;
using UnityEngine;
using UnityEngine.Localization;

namespace Gameplay.Score.Widgets
{
    public class ScoreWidget : MonoBehaviour, IScoreWidget
    {
        [SerializeField] private TMP_Text _field;
        [SerializeField] private LocalizedString _localizedString;

        private void OnEnable()
        {
            _localizedString.Arguments = new object[] {"score"};
            _localizedString.StringChanged += UpdateText;
        }

        private void UpdateText(string value)
        {
            _field.text = value;
        }

        private void OnDisable()
        {
            _localizedString.StringChanged -= UpdateText;
        }

        public void SetScore(int value)
        {
            _localizedString.Arguments[0] = value;
            _localizedString.RefreshString();
        }
    }
}
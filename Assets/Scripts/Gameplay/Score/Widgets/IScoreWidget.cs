namespace Gameplay.Score.Widgets
{
    public interface IScoreWidget
    {
        public void SetScore(int value);
    }
}
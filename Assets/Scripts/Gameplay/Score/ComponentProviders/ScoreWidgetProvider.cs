using Gameplay.Score.Components;
using Gameplay.Score.Widgets;
using UnityEngine;
using Voody.UniLeo;

namespace Gameplay.Score.ComponentProviders
{
    [RequireComponent(typeof(IScoreWidget))]
    public class ScoreWidgetProvider : MonoProvider<ScoreWidgetComponent>
    {
        private void Awake()
        {
            value.Value = GetComponent<IScoreWidget>();
        }
    }
}
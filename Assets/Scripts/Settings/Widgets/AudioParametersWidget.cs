using Asp.Runtime;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Settings.Widgets
{
    public class AudioParametersWidget : MonoBehaviour, IInitializable
    {
        private IAudioParameters _audioParameters;
        [SerializeField] private UnityEvent<bool> OnMusicEnabledChanged;
        [SerializeField] private UnityEvent<float> OnMusicVolumeChanged;
        [SerializeField] private UnityEvent<bool> OnSfxEnabledChanged;
        [SerializeField] private UnityEvent<float> OnSfxVolumeChanged;

        public bool MusicEnabled
        {
            set => _audioParameters.MusicEnabled = value;
        }

        public bool SfxEnabled
        {
            set => _audioParameters.SfxEnabled = value;
        }

        public float MusicVolume
        {
            set => _audioParameters.MusicVolume = value;
        }

        public float SfxVolume
        {
            set => _audioParameters.SfxVolume = value;
        }

        [Inject]
        private void Construct(IAudioParameters audioParameters)
        {
            _audioParameters = audioParameters;
            UpdateWidgets();
        }

        public void Initialize()
        {
            UpdateWidgets();
        }

        private void UpdateWidgets()
        {
            OnMusicEnabledChanged?.Invoke(_audioParameters.MusicEnabled);
            OnMusicVolumeChanged?.Invoke(_audioParameters.MusicVolume);
            OnSfxEnabledChanged?.Invoke(_audioParameters.SfxEnabled);
            OnSfxVolumeChanged?.Invoke(_audioParameters.SfxVolume);
        }
    }
}
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Settings.Widgets
{
    public class SettingWidget : MonoBehaviour
    {
        [SerializeField] private Button _closeButton;

        public Action OnClose { get; set; }

        private void Start()
        {
            _closeButton.onClick.AddListener(() => OnClose?.Invoke());
        }
    }
}
using Common.Constants;
using Common.Services.AssetLoading.AssetLoaders;
using Settings.Startup;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

namespace Settings.Widgets
{
    public class SettingsButtonWidget : MonoBehaviour
    {
        [SerializeField] private Transform _settingsWidgetParent;
        [SerializeField] private Button _button;
        [SerializeField] private UnityEvent _onOpenSettings;
        [SerializeField] private UnityEvent _onCloseSettings;
        private IAssetLoader<GameObject> _loader;
        private SettingWidget _settingsWidget;
        private SettingsWidgetFactory _settingsWidgetFactory;

        [Inject]
        private void Construct(SettingsWidgetFactory settingsWidgetFactory)
        {
            _settingsWidgetFactory = settingsWidgetFactory;
        }
        
        private void Start()
        {
            _loader = new AssetLoaderByKey<GameObject>(PrefabNames.SettingsPrefab);
            _button.onClick.AddListener(OpenSettings);
        }

        private async void OpenSettings()
        {
            _settingsWidget = await _settingsWidgetFactory.Create(_loader, _settingsWidgetParent);
            _settingsWidget.OnClose += Dispose;
            _onOpenSettings.Invoke();
        }

        private void Dispose()
        {
            _settingsWidget.OnClose -= Dispose;
            Destroy(_settingsWidget.gameObject);
            _loader.Dispose();
            _onCloseSettings.Invoke();
        }

        private void OnDestroy()
        {
            _loader.Dispose();
            _button.onClick.RemoveListener(OpenSettings);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common.Services.Localization;
using TMPro;
using UnityEngine;
using UnityEngine.Localization.Settings;
using Zenject;

namespace Settings.Widgets
{
    [RequireComponent(typeof(TMP_Dropdown))]
    public class LanguageDropdownWidget : MonoBehaviour
    {
        private TMP_Dropdown _dropdown;
        private ILocaleIndexService _localeIndexService;

        [Inject]
        private void Construct(ILocaleIndexService  localeIndexService)
        {
            _localeIndexService = localeIndexService;
        }
        
        private void Awake()
        {
            _dropdown = GetComponent<TMP_Dropdown>();
        }

        private IEnumerator Start()
        {
            yield return LocalizationSettings.InitializationOperation;
            
            _dropdown.options = GetAvailableLocalesList();
            _dropdown.value = _localeIndexService.LocaleIndex;
            _dropdown.onValueChanged.AddListener(LocaleSelected);
        }

        private static List<TMP_Dropdown.OptionData> GetAvailableLocalesList()
        {
            return LocalizationSettings.AvailableLocales.Locales
                .Select(locale => new TMP_Dropdown.OptionData(locale.name)).ToList();
        }

        private static int GetSelectedIndex()
        {
            return LocalizationSettings.AvailableLocales.Locales
                .FindIndex(e => e == LocalizationSettings.SelectedLocale);
        }

        private void LocaleSelected(int index)
        {
            _localeIndexService.LocaleIndex = index;
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[index];
            Debug.Log($"LocaleSelected {LocalizationSettings.SelectedLocale.LocaleName} {index}");
        }
    }
}

using System.Threading.Tasks;
using Asp.Runtime;
using Common.Services.AssetLoading.AssetLoaders;
using Common.Services.Localization;
using Settings.Widgets;
using UnityEngine;
using Zenject;

namespace Settings.Startup
{
    public class SettingsWidgetFactory : PlaceholderFactory<ILocaleIndexService, IAudioParameters, IAssetLoader<GameObject>, Transform, SettingWidget>
    {
        private readonly DiContainer _container;

        public SettingsWidgetFactory(DiContainer container)
        {
            _container = container;
        }


        public async Task<SettingWidget> Create(IAssetLoader<GameObject> loader, Transform parent)
        {
            var prefab = await loader.Load();
            var obj = _container.InstantiatePrefab(prefab, parent);
            return obj.GetComponent<SettingWidget>();
            
        }
        
    }
}